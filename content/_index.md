---
title: MapRando
subtitle: Carte adaptée à la randonnée
---


{{< gallery hover-effect="none" >}}
  {{< figure src="./export_1.png" >}}
  {{< figure src="./export_2.png" >}}
  {{< figure src="./export_3.png" >}}
  {{< figure src="./export_4.png" >}}
  {{< figure src="./export_5.png" >}}
  {{< figure src="./export_6.png" >}}
{{< /gallery >}}

## Pourquoi utiliser cette carte ?
La carte fournie par défaut par Garmin est TopoActive, elle est basée sur OpenStreetMap et Garmin a créé son propre style.
Dans leur choix ils ont supprimé certains chemins tels que des chemins trop petit ou non praticable car la carte est orientée plus pour des activités sur route ou gros chemin, c'est pour cela que Garmin vend TopoPro V5 qui est une carte plus adaptée à la pratique de la randonnée en montagne. Elle est basé sur la base de données IGN et non OpenStreetMap, il est donc possible que des chemins soient différents voire existent sur l'une et pas sur l'autre.

Cette carte peut convenir à beaucoup de monde mais le manque de chemins ou de mise à jour et le prix peut être un frein, c'est pour cela que le carte basée sur OpenStreetMap sont plus adaptée.

## Téléchargement
#### Nom des fichiers
Les fichiers respectent un nommage MapRando suivi de la région suivi de la date de la dernière mise à jour.

Ce qui donne par exemple pour la carte de rando france faite le 1er janvier 2021 => **MapRando_France_2021_01_01.img**


[Téléchargement des cartes](http://157.173.105.20/MapRando)
  
Vous souhaitez une région en plus, n'hésitez pas à envoyer un email.

{{<alert theme="info" >}}**Info:** Les cartes sont disponible sur une serveur qui n'est pas gratuit (100€ a l'année). Merci aux contributions{{</alert>}}

## Installation
Pour installer une nouvelle carte, copier le fichier img dans le répertoire GARMIN de votre montre.

Il ne vous reste plus qu'a activé la carte sur les profils dont vous souhaitez l'utiliser. Vous pouvez 
déselectionnez toutes les cartes d'une même région sauf DEM (si votre appareil Garmin vous l'affiche) et la carte que vous voulez afficher. 

Si vous en activez plusieurs, l'ordre d'affichage se fait en fonction d'un numéro indiqué dans le fichier de la carte que seul le développeur connaît, même si sur l'écran vous avez la bonne carte au niveau du rendu il va dessiner les différentes cartes et du coup utiliser plus de processeur.

Pour plus de détails: https://www.montre-cardio-gps.fr/carto-sur-une-montre-gps-garmin-installation-configuration-et-utilisation/

{{<alert theme="warning" >}}**Les thèmes garmin:** Sur certains modèles vous pouvez avoir des thèmes sur les cartes. Certain thèmes ne rendent pas la carte très lissible, il est recommandé de mettre aucun thème{{</alert>}}


{{<alert theme="info" >}}**Astuce:** Sur certains modèles si vous renommez le fichier en supprimant la date par exemple, lors de la prochaine mise à jour celui-ci sera remplacé et vous n'aurez pas besoin d'activer la nouvelle carte sur vos activités. {{</alert>}}

Merci Jérôme pour la video:

{{< youtube "48_PlcPg3a0" >}}

## Mise à jour 
Si possible en début de mois, si vous remarquez des incohérences entre une carte avec le fournisseur de données OpenStreetMap et ma carte ou si vous êtes contributeur et que vous voulez retrouver vos modifications n'hésitez pas à me contacter pour que je lance une mise à jour.

## Me soutenir
Je ne cherche pas à gagner ma vie avec ce projet, mais vous pouvez me payer une bière c'est avec plaisir
[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/T6T86HANA)

## Mes applications Connect IQ

- https://apps.garmin.com/fr-FR/developer/9a164185-3030-48d9-9aef-f5351abe70d8/apps


## Autres cartes
Vous trouvez sur le wiki d'OpenStreetMap la plupart des cartes basées sur OpenStreetMap 
- https://wiki.openstreetmap.org/wiki/FR:OSM_Map_On_Garmin/Download

Pour Garmin
- https://buy.garmin.com/fr-FR/FR/c452-c456-p1.html

## Les sources
Tout se trouve sur gitlab, mes scripts, rendue et une explication si vous voulez faire la même chose.
https://gitlab.com/ravenfeld/garmincustommap









