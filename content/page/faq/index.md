---
title: FAQ
---

## J'ai un Mac et ma montre n'est pas reconnu.
La montre utilise le format MTP qui n'est pas reconnu par défault sur Mac OS, il faut donc installer un logiciel telque [OpenMTP](https://openmtp.ganeshrvel.com/) .

Petite astuce, brancher votre montre et attendez 20 à 30 secondes avant d'ouvrir OpenMTP.

## La carte ne s'affiche pas ?
Vous ne devez pas avoir plusieurs carte d'activé pour une activité, déselectionné toute vos cartes sauf DEM et la carte que vous voulez afficher.

Si vous en activez plusieurs l'ordre d'affichage se fait en fonction d'un numéro indiqué dans le fichier de la carte que seul le développeur connait, même si sur l'écran vous avez la bonne carte au niveau du rendu il va dessiner les différentes cartes et du coup utiliser plus de processeur.

## Est elle routable ?
Oui c'est une carte routable, c'est à dire que la fonctionne pour faire en boucle un itinéaire ou pour vous guider jusqu'a un point fontionne.

Mais celle ci n'est pas optimisé, comme c'est encore une fonction qui sur les montres n'est pas très utile de mon point de vue ce point n'a pas été approfondie. Il est donc possible qu'elle vous propose un chemin en cailloux alors que vous etes en vélo de route. Le raisonnement est à l'inverse de Garmin, pour moi tout chemin est praticable sauf si il est indiqué dans la base de données. 

## J'ai installé 2 cartes d'une même région mais la montre en détecte qu'une pourquoi ?
Les cartes ont un identifiant et du coup la montre ne peut charger qu'une carte avec le même identifiant. C'est pour cela que vous ne pouvez installer deux cartes d'une même région.

Exemple: 
MapRando_France_2022_09_01.img et MapRando_France_2022_10_04.img ont le même identifiant, la montre ne verra qu'une carte souvent la 1er qui a été installé. 

## Pourquoi je dois désactiver les autres cartes ?
Je recommande de désactiver les autres cartes d'une même région car si plusieurs cartes sont sur la même région alors la montre va dessiner les cartes dans une ordre défini par les dévéloppeurs. Cela implique donc du temps de calcul à votre montre qui peut etre inutile. 

Exemple: J'ai Topo Active et MapRando, la montre va dessiner Topo Active puis par dessus MapRando. MapRando étant un rendu et non un calque avoir pris du temps pour dessiner Topo Active était donc inutile.

{{<alert theme="info" >}}**Info:** Cetain appareil avec Topo Active peuvent avoir les ombrages qui est un calque contenu dans le fichier de Topo Active, si vous avez envie d'avoir les ombrages alors il faudra laisser Topo Active d'activé. Cela est donc un compromis de votre part entre rendu vs consommation.{{</alert>}}

## Est ce que je peux activer pluiseurs cartes en même temps ?
OUI

Si l'on parle de carte = région, alors oui je n'ai aucune récommandation la dessus, la montre gère très bien ca. 

Exemple: Je veux avoir la France et l'Italie. Il me suffit d'installer et d'activer la France et l'Italie.

## Le fichier est considéré endommagé par Windows ?
Le fichier n'est pas fait pour être ouvert par votre ordinateur mais par votre montre. 

Si vous souhaitez l'ouvrir sur votre ordinateur il existe le logiciel [QMapShack](https://github.com/Maproom/qmapshack/wiki), très pratique pour comparer les différences entre les cartes mais attention les couleurs ne sont pas forcément représentatif de votre montre car votre écran d'ordinateur en gère beaucoup plus.

## Pourquoi mon chemin n'existe pas sur la carte ?
La carte est basée sur la base de données OpenStreetMap, bien qu'il y est des milliers de contributeurs il est possible que dans votre zone il en manque ou qu'ils ne visitent pas les mêmes endroits que vous. Si vous voulez une carte plus précise, ne comptez pas sur les autres mais contribuez vous aussi. Si chacun fait les chemins autour de chez lui nous y gagnerons tous.

Pour contribuer: https://www.openstreetmap.fr/contribuer

## Pourquoi les couleurs ne correspondent pas à la légende ?
Il est possible que vous ayez activé un thème pour une autre carte comme Popularité par exemple, il faut donc pour votre activité où vous activez la carte choisir le thème Aucun. 

Ne pas oublier le mode Jour/Nuit qui est aussi un thème. Il faut mettre sur Jour en permanance car le mode nuit n'est pas géré.

## Je ne sais pas ce que couvre exactement la zone pour un pays donnée.
J'utilise principalement le site [GeoFabrik](https://download.geofabrik.de/) vous pouvez donc regarder exactement ce que contient tel ou tel pays.

## Je voudrais utiliser le rendu sur mon ordinateur comment faire ?
C'est une rendu pour la montre, qui reprend les données d'OpenStreetMap. Je ne génère pas de fichier pour BaseCamp car pour moi c'est une logiciel dépassé et qu'il existe beaucoup mieux.

Désolé pour ceux qui veulent l'utiliser mais au pire vous pouvez utiliser le rendu si vous laissez votre appareil connecté.

## Je voudrais supprimer des fichiers pour avoir plus d'espace.
La question a se poser est "Pourquoi j'en besoin d'espace ?"
1. Vous essayez d'installer sur une montre qui n'a pas la fonctionnalité de la cartographie et donc peu d'espace
2. Pour x raisons (Musique, plusieurs cartes) vous n'avez pas assez d'espace.

Pour le point 1, ca sert a rien votre montre est tout simple pas compatible.

Pour le point 2,
* si vous avez une montre avec Map Manager alors supprimez les cartes via cette outil [video tuto](https://www.youtube.com/watch?v=K8z9jT5Lqjs)
* si vous n'avez pas une montre avec Map Manager alors attention car supprimer des fichiers directement peut bloquer votre montre. Je vous conseille de supprimer que des fichiers .img normalement au pire des cas cela sera réinstallé par Garmin Express. 

Cela reste a vos risques, pour la petite histoire pour avoir "bidouillé" avec ma montre j'ai du la remplacé 4 fois car était devenu une brique. 

![fichier](./name_file.webp)


